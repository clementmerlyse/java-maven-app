def buildJar() {
    echo "building the application..."
    sh 'mvn package'
}
def buildImage() {
    echo "building the docker image ... "
    withCredentials([usernamePassword(credentialsId: 'id-docker-hub',passwordVariable: 'PWD',usernameVariable:'USER')]){
        sh 'docker build -t clementmerlyse/jenkins-repo:class-jma-1.30 .'
        sh "echo $PWD | docker login -u $USER --password-stdin"
        sh 'docker push clementmerlyse/jenkins-repo:class-jma-1.30'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this